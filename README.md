# eshop-payment


1)
    * Implementare observer pattern (clase -> Client)  
    * Implementare modele (clase -> CardPaymentMethod, PaymentResult)
         
     
2)   
    * Implementare singleton pattern + repository (clase -> InMemoryClientRepository)
    * Implementare builder pattern (clase -> PaymentRequest)
    
    
3)
    * Implemenatre template pattern (clase -> PaymentProcessorTemplate)
    
    
4)
    * Implementare strategy pattern (clase -> CardPaymentStrategy, CashOnDeliveryPaymentStrategy)  
    * Implemenatare factory pattern (clase -> PaymentFactoryImpl)
    
+ Documentatie
    

    

    
