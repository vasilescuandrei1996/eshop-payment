package ro.ase.ebusiness;



import java.time.LocalDateTime;


import ro.ase.ebusiness.payment.exception.PaymentException;
import ro.ase.ebusiness.payment.model.CardPaymentMethod;
import ro.ase.ebusiness.payment.model.CashOnDeliveryPaymentMethod;
import ro.ase.ebusiness.payment.model.PaymentRequest;
import ro.ase.ebusiness.payment.model.PaymentRequest.PaymentRequestBuilder;
import ro.ase.ebusiness.payment.processor.PaymentProcessorTemplate;


public class EshopPaymentApplication {

    public static void main(String[] args) {


        PaymentProcessorTemplate paymentProcessorTemplate = new PaymentProcessorTemplate();
        PaymentRequestBuilder paymentRequestBuilder = new PaymentRequestBuilder();
        PaymentRequest payment1 = paymentRequestBuilder
                .setAmount(25)
                .setClientId(1)
                .setDate(LocalDateTime.now())
                .setShipmentAddress("Bucharest")
                .setMethod(new CardPaymentMethod("ROINGB1129321832183100009990","Mihai Vasile","12/24","123"))
                .build();

        PaymentRequest payment2 = paymentRequestBuilder
                .setAmount(60)
                .setClientId(2)
                .setDate(LocalDateTime.now())
                .setShipmentAddress("Bucharest Sect 6")
                .setMethod(new CashOnDeliveryPaymentMethod())
                .build();
        try {
            paymentProcessorTemplate.process(payment1);

        } catch (PaymentException e) {
            System.out.println(String.format("Eroare la procesarea platii: %s", e.getMessage()));
        }

        try {
            paymentProcessorTemplate.process(payment2);

        } catch (PaymentException e) {
            System.out.println(String.format("Eroare la procesarea platii: %s", e.getMessage()));
        }

    }
}
