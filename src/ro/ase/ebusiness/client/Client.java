package ro.ase.ebusiness.client;

import ro.ase.ebusiness.payment.model.PaymentResult;

public class Client implements ClientObserver {

    private long id;
    private String firstName;
    private String lastName;
    
    
    public Client(long id, String firstName, String lastName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	@Override
    public void notify(PaymentResult paymentResult) {
		paymentResult.printPaymentResult();
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
