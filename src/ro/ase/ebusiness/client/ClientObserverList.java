package ro.ase.ebusiness.client;

import java.util.ArrayList;

public abstract class ClientObserverList {		
	
	ArrayList<ClientObserver> observers = new ArrayList<ClientObserver>();

    public ClientObserverList(){

    }

    public void addObserver(ClientObserver observer){
        observers.add(observer);

    }

    public void removeObserver(ClientObserver observer){
        observers.remove(observer);
    }

    public void notifyObservers(){
        for(int i=0; i < observers.size(); i++){
            observers.get(i).notify();
        }
        
    }

}
