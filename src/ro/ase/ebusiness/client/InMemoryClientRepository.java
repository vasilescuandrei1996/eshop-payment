package ro.ase.ebusiness.client;

import java.util.ArrayList;
import java.util.List;

public class InMemoryClientRepository implements ClientRepository {

	private static InMemoryClientRepository instance;
	private List<Client> clients;

	private InMemoryClientRepository() {
		init();
	}

	private void init() {
		clients = new ArrayList<>();
		clients.add(new Client(1,"Mihai","Vasile"));
		clients.add(new Client(2,"George","Alex"));
		clients.add(new Client(3,"Alex","Costel"));
		clients.add(new Client(4,"Costel","Nae"));
		clients.add(new Client(5,"Claudiu","Xan"));
		clients.add(new Client(6,"Bogdan","Gheorge"));
		clients.add(new Client(7,"Liviu","Ana"));
		clients.add(new Client(8,"Ana","Putna"));
		clients.add(new Client(9,"Maria","Ilean"));
		clients.add(new Client(10,"Ileana","Nastase"));
		clients.add(new Client(11,"Grigore","Alecsadri"));
		clients.add(new Client(12,"Vlad","Mihalcea"));
		clients.add(new Client(13,"Sofia","Dan"));
		clients.add(new Client(14,"Ximena","Ene"));
		clients.add(new Client(15,"Alex","Vacarescu"));
		clients.add(new Client(16,"Bianca","Barbu"));
	}

	public static InMemoryClientRepository getInstance() {
		if (instance == null)
			instance = new InMemoryClientRepository();

		return instance;
	}

	@Override
	public Client getClientById(long id) {
		return clients.stream().filter(client -> client.getId() == id).findFirst().orElse(null);
	}
}
