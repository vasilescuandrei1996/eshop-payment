package ro.ase.ebusiness.client;

public class ClientObserverManager extends ClientObserverList {

	public ClientObserverManager() {
		super();
	}
	
	public void sendNotification() {
		System.out.println("Clientii urmeaza sa fie notifcati cu privire la tranzactii");
		this.notifyObservers();
	}

}
