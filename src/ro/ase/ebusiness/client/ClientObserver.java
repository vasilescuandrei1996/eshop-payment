package ro.ase.ebusiness.client;

import ro.ase.ebusiness.payment.model.PaymentResult;


public interface ClientObserver {

    void notify(PaymentResult paymentResult);

}
