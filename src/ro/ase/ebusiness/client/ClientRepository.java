package ro.ase.ebusiness.client;


public interface ClientRepository {

    Client getClientById(long id);

}
