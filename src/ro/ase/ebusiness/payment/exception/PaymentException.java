package ro.ase.ebusiness.payment.exception;


public class PaymentException extends Exception {

    private String message;

    public PaymentException(String message) {
        super(message);
    }
}
