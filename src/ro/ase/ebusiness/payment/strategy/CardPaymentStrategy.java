package ro.ase.ebusiness.payment.strategy;

import ro.ase.ebusiness.client.Client;
import ro.ase.ebusiness.payment.model.CardPaymentMethod;
import ro.ase.ebusiness.payment.model.PaymentRequest;
import ro.ase.ebusiness.payment.model.PaymentResult;


public class CardPaymentStrategy implements PaymentStrategy {

    @Override
    public PaymentResult pay(Client client, PaymentRequest paymentRequest) {
        System.out.println();
        System.out.println(String.format("Urmeaza sa se proceseze plata pentru clientul %s %s.", client.getFirstName(), client.getLastName()));
        CardPaymentMethod cardPaymentMethod = (CardPaymentMethod) paymentRequest.getMethod();
        System.out.println(String.format("Suma %f a fost retrasa din contul %s.", paymentRequest.getAmount(), cardPaymentMethod.getIBAN()));
        return new PaymentResult("success");
    }
}
