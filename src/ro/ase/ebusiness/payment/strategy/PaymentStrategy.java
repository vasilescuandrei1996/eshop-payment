package ro.ase.ebusiness.payment.strategy;

import ro.ase.ebusiness.client.Client;
import ro.ase.ebusiness.payment.model.PaymentRequest;
import ro.ase.ebusiness.payment.model.PaymentResult;


public interface PaymentStrategy {

    PaymentResult pay(Client client, PaymentRequest paymentRequest);
}
