package ro.ase.ebusiness.payment.strategy;

import ro.ase.ebusiness.payment.exception.PaymentException;
import ro.ase.ebusiness.payment.model.PaymentType;


public class PaymentFactoryImpl implements PaymentFactory {

    @Override
    public PaymentStrategy getStrategy(PaymentType paymentType) throws PaymentException {
        if(PaymentType.CARD.equals(paymentType)) {
            return new CardPaymentStrategy();
        }
        if(PaymentType.CASH_ON_DELIVERY.equals(paymentType)) {
            return new CashOnDeliveryPaymentStrategy();
        }
        throw new PaymentException("Nu s-a putut determina strategia de plata");
    }
}
