package ro.ase.ebusiness.payment.strategy;

import ro.ase.ebusiness.payment.exception.PaymentException;
import ro.ase.ebusiness.payment.model.PaymentType;


public interface PaymentFactory {

    PaymentStrategy getStrategy(PaymentType paymentType) throws PaymentException;
}
