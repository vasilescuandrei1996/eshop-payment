package ro.ase.ebusiness.payment.strategy;

import ro.ase.ebusiness.client.Client;
import ro.ase.ebusiness.payment.model.PaymentRequest;
import ro.ase.ebusiness.payment.model.PaymentResult;


public class CashOnDeliveryPaymentStrategy implements PaymentStrategy {

    @Override
    public PaymentResult pay(Client client, PaymentRequest paymentRequest) {
        System.out.println();
        System.out.println(String.format("Urmeaza sa se proceseze plata pentru clientul %s %s.", client.getFirstName(), client.getLastName()));
        System.out.println(String.format("Suma %f va fi achitata de catre client in momentul primirii coletului la adresa %s.", paymentRequest.getAmount(), paymentRequest.getShipmentAddress()));
        return new PaymentResult("pending");
    }
}
