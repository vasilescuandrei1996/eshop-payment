package ro.ase.ebusiness.payment.processor;

import ro.ase.ebusiness.client.Client;
import ro.ase.ebusiness.client.ClientObserverManager;
import ro.ase.ebusiness.client.ClientRepository;
import ro.ase.ebusiness.client.InMemoryClientRepository;
import ro.ase.ebusiness.payment.exception.PaymentException;
import ro.ase.ebusiness.payment.model.PaymentRequest;
import ro.ase.ebusiness.payment.model.PaymentResult;
import ro.ase.ebusiness.payment.strategy.PaymentFactory;
import ro.ase.ebusiness.payment.strategy.PaymentFactoryImpl;
import ro.ase.ebusiness.payment.strategy.PaymentStrategy;

public class PaymentProcessorTemplate extends AbstractPaymentProcessorTemplate {

	private ClientRepository clientRepository;
	private PaymentFactory paymentFactory;

	public PaymentProcessorTemplate() {
		clientRepository = InMemoryClientRepository.getInstance();
		paymentFactory = new PaymentFactoryImpl();
	}

	@Override
	protected void validatePayment(PaymentRequest paymentRequest) throws PaymentException {
		if (paymentRequest.getClientId() < 0) {
			throw new PaymentException("Clientul nu a fost gasit");
		}
		if (paymentRequest.getShipmentAddress() == null) {
			throw new PaymentException("Adresa de livrare nu a fost gasita");
		}
		if (paymentRequest.getAmount() < 0) {
			throw new PaymentException("Suma de plata nu poate fi mai mica de 0");
		}
	}

	@Override
	protected Client getClient(PaymentRequest paymentRequest) throws PaymentException {
		return clientRepository.getClientById(paymentRequest.getClientId());
	}

	@Override
	protected PaymentStrategy getPaymentStrategy(PaymentRequest paymentRequest) throws PaymentException {
		return paymentFactory.getStrategy(paymentRequest.getMethod().getType());
	}

	@Override
	protected void notifyClient(Client client, PaymentResult result) throws PaymentException {
		 client.notify(result);
	}
}
