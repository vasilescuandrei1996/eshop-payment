package ro.ase.ebusiness.payment.processor;

import ro.ase.ebusiness.client.Client;
import ro.ase.ebusiness.payment.exception.PaymentException;
import ro.ase.ebusiness.payment.model.PaymentRequest;
import ro.ase.ebusiness.payment.model.PaymentResult;
import ro.ase.ebusiness.payment.strategy.PaymentStrategy;


public abstract class AbstractPaymentProcessorTemplate {

    public void process(PaymentRequest paymentRequest) throws PaymentException {
        validatePayment(paymentRequest);
        Client client = getClient(paymentRequest);
        PaymentStrategy strategy = getPaymentStrategy(paymentRequest);
        PaymentResult result = strategy.pay(client, paymentRequest);
        notifyClient(client, result);
    }

    protected abstract void validatePayment(PaymentRequest paymentRequest) throws PaymentException;
    protected abstract Client getClient(PaymentRequest paymentRequest) throws PaymentException;
    protected abstract PaymentStrategy getPaymentStrategy(PaymentRequest paymentRequest) throws PaymentException;
    protected abstract void notifyClient(Client client, PaymentResult result) throws PaymentException;

}
