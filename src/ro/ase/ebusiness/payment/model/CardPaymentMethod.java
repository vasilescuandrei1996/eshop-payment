package ro.ase.ebusiness.payment.model;


public class CardPaymentMethod extends PaymentMethod {

    private String IBAN;
    private String clientName;
    private String expirationDate;
    private String CVV;

    public CardPaymentMethod(String IBAN, String clientName, String expirationDate, String CVV) {
        super(PaymentType.CARD);
        this.IBAN = IBAN;
        this.clientName = clientName;
        this.expirationDate = expirationDate;
        this.CVV = CVV;
    }

    public String getIBAN() {
        return IBAN;
    }

    public String getClientName() {
        return clientName;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public String getCVV() {
        return CVV;
    }
}
