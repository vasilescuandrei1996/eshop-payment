package ro.ase.ebusiness.payment.model;

import java.time.LocalDateTime;

public class PaymentRequest {

	private long clientId;
	private String shipmentAddress;
	private LocalDateTime date;
	private double amount;
	private PaymentMethod method;

	// implementare builder
	public static class PaymentRequestBuilder {
		private long clientId;
		private String shipmentAddress;
		private LocalDateTime date;
		private double amount;
		private PaymentMethod method;

		public PaymentRequestBuilder setClientId(long clientId) {
			this.clientId = clientId;
			return this;
		}

		public PaymentRequestBuilder setShipmentAddress(String shipmentAddress) {
			this.shipmentAddress = shipmentAddress;
			return this;
		}

		public PaymentRequestBuilder setDate(LocalDateTime date) {
			this.date = date;
			return this;
		}

		public PaymentRequestBuilder setAmount(double amount) {
			this.amount = amount;
			return this;
		}

		public PaymentRequestBuilder setMethod(PaymentMethod method) {
			this.method = method;
			return this;
		}

		public PaymentRequest build() {
			return new PaymentRequest(clientId, shipmentAddress, date, amount, method);
		}
	}

	private PaymentRequest(long clientId, String shipmentAddress, LocalDateTime date, double amount,
			PaymentMethod method) {
		super();
		this.clientId = clientId;
		this.shipmentAddress = shipmentAddress;
		this.date = date;
		this.amount = amount;
		this.method = method;
	}

	private PaymentRequest() {
	}

	public long getClientId() {
		return clientId;
	}

	public String getShipmentAddress() {
		return shipmentAddress;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public double getAmount() {
		return amount;
	}

	public PaymentMethod getMethod() {
		return method;
	}
}
