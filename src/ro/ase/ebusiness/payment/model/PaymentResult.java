package ro.ase.ebusiness.payment.model;

public class PaymentResult {

	private String status;
	
	
	public PaymentResult(String status) {
		this.status = status;
	}
	
	public void printPaymentResult() {
		
		if(this.status.equals("pending")) {
			System.out.println( "Notificare client: Tranzactia dumneavoastra este in procesare!");
		}
		if(this.status.equals("success")) {
			System.out.println("Notificare client: Tranzactia dumneavoastra a fost finalizata cu success!");
		}
		
	}

	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
