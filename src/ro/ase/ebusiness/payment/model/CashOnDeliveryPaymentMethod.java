package ro.ase.ebusiness.payment.model;


public class CashOnDeliveryPaymentMethod extends PaymentMethod {

    public CashOnDeliveryPaymentMethod() {
        super(PaymentType.CASH_ON_DELIVERY);
    }
}
