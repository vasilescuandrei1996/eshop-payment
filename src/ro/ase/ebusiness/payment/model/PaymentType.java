package ro.ase.ebusiness.payment.model;


public enum PaymentType {
    CARD,
    CASH_ON_DELIVERY
}
