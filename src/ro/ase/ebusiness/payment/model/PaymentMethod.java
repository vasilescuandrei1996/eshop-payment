package ro.ase.ebusiness.payment.model;


public abstract class PaymentMethod {

    protected PaymentType type;

    public PaymentMethod(PaymentType type) {
        this.type = type;
    }

    public PaymentType getType() {
        return type;
    }
}
